Changelog
===============================
### May 2022
* Updated text for which time period we have ads
* Show single ad from search result html-formatted

### February 2022
* Format dates


### January 2022
* Changed css file and removed unused files
* Single page with Javascript search
* Search with free text query and optional from- and to-dates. Default seting to only show number of ads and positions
